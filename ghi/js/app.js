function createCard(name, location, description, pictureUrl, starts, ends) {
  return `
  <div class="shadow m-4">
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <small class="text-muted">${location}</small>
        <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${starts} - ${ends}
        </div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name

          const startDate = new Date(details.conference.starts)
          const starts = startDate.toLocaleDateString()

          const endDate = new Date(details.conference.ends)
          const ends = endDate.toLocaleDateString()

          const html = createCard(title, location, description, pictureUrl, starts, ends);
          const column = document.querySelector('.col');
          column.innerHTML += html;
        }
      }

    }
  } catch (e) {
    console.error(e);
    // Figure out what to do if an error is raised
  }

});
