import React from "react";
import Nav from "./Nav";

function App(props) {
  return (
    <React.Fragment>
    <Nav />
    <div className="container">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
        {props.attendees.map(attendee => {
          return (
            <tr key={attendee.href}>
              <td>{ attendee.name }</td>
              <td>{ attendee.conference }</td>
            </tr>
          );
        })}
        </tbody>
      </table>
    </div>
    </React.Fragment>
  );
}

export default App;
